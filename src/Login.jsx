import React, { Component } from "react";
import Message from "./components/Message";
import axios from "axios";

import "antd/dist/antd.css";
import "./styles/login.css";
import AuthContext from "./context/auth-context";
import Cookies from "js-cookie";

const API = "https://mhtoolsapi.iot.mi.com";

class Login extends Component {
  state = {
    isLogin: true,
    message: null,
  };

  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.username = React.createRef();
    this.password = React.createRef();
  }

  switchModeHandler = (e) => {
    this.setState((prevState) => {
      return { isLogin: !prevState.isLogin };
    });
  };

  submitHandler = async (e) => {
    e.preventDefault();
    const username = this.username.current.value;
    const password = this.password.current.value;

    if (username.trim().length === 0 || password.trim().length === 0) {
      return;
    }

    try {
      const res = await axios.post(
        /*"http://localhost:3000/login"*/ `${API}/login`,
        {
          params: {
            username: username,
            password: password,
          },
        }
      );
      if (res.status === 200) {
        console.log(res.data);
        if (this.state.isLogin && res.data.token) {
          this.context.login(res.data.token, res.data.tokenExpiration);
          Cookies.set("token", res.data.token);
          Cookies.set("tokenExpiration", res.data.tokenExpiration);
        }
      }
    } catch (err) {
      this.setState({ message: "用户名与密码不匹配！" });
    }

    /*let requestBody = {
            query: `
                query {
                    login(email: "${username}", password: "${password}") {
                        id
                        token
                        tokenExpiration
                    }
                }
            `
        };*/

    /*if (!this.state.isLogin) {
            requestBody = {
                query: `
                    mutation {
                        addUser(email: "${username}", password: "${password}") {
                            id
                            token
                            tokenExpiration
                        }
                    }
                `
            };
        }


        fetch('https://crazyfitapi.herokuapp.com/graphql', {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201) {
                throw new Error('Failed!');
            }
            return res.json();
        })

        .then(resData => {
            console.log(resData);
            if (this.state.isLogin && resData.data.login.token) {
                this.context.login(
                    resData.data.login.token,
                    resData.data.login.id,
                    resData.data.login.tokenExpiration
                );
                // Cookies.set("token", resData.data.login.token);
                // Cookies.set("id", resData.data.login.id);
                // Cookies.set("tokenExpiration", resData.data.login.tokenExpiration);
                console.log([this.context.id, this.context.token]);
                
            } else if (!this.state.isLogin && resData.data.addUser.token) {
                this.context.login(
                    resData.data.addUser.token,
                    resData.data.addUser.id,
                    resData.data.addUser.tokenExpiration
                );
                // Cookies.set("token", resData.data.addUser.token);
                // Cookies.set("id", resData.data.addUser.id);
                // Cookies.set("tokenExpiration", resData.data.addUser.tokenExpiration);
            }
        })
        .catch(err => {
            console.log(err);
        });*/
  };

  readCookie = () => {
    // const token = Cookies.get("token");
    // if (token) {
    // this.context.token = token;
    // }
  };

  render() {
    return (
      <div style={{}}>
        <div style={{ width: "100%" }}>
          <header
            className="site-layout-sub-header-background"
            style={{
              height: "3em",
              textAlign: "center",
              lineHeight: "3em",
              fontSize: "1.5em",
              fontWeight: 700,
              backgroundColor: "rgb(37, 43, 57)", // "rgb(94, 94, 163)",
              color: "#fff",
            }}
          >
            米家工具
          </header>
          {this.state.message ? <Message msg={this.state.message} /> : null}
          <form className="auth-form" onSubmit={this.submitHandler}>
            <div className="form-control">
              <label htmlFor="username">用户名</label>
              <input
                type="text"
                id="username"
                ref={this.username}
                style={{
                  height: "2.5em",
                }}
              />
            </div>
            <div className="form-control">
              <label htmlFor="password">密码</label>
              <input
                type="password"
                id="password"
                ref={this.password}
                style={{ height: "2.5em" }}
              />
            </div>
            <div
              className="form-actions"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <button type="submit">提交</button>
              <button type="button" onClick={this.switchModeHandler}>
                {this.state.isLogin ? "注册" : "登录"}
              </button>
            </div>
          </form>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
          }}
        >
          <footer
            style={{
              position: "absolute",
              bottom: "2em",
              textAlign: "center",
              fontSize: "1.5em",
              color: "gray",
            }}
          >
            MIOT
          </footer>
        </div>
      </div>
    );
  }
}

export default Login;
