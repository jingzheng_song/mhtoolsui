import React from "react";

import { Layout, Menu, Button } from "antd";
import { FileZipOutlined, InboxOutlined } from "@ant-design/icons";
import FileUpload from "./components/FileUpload";
import Pack from "./components/Pack";

import AuthContext from "./context/auth-context";

import axios from "axios";
import "antd/dist/antd.css";
import "./styles/home.css";

const { Header, Content, Sider, Footer } = Layout;
const API = "https://mhtoolsapi.iot.mi.com";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      filePath: "",
      key: "",
      isLogDecrypting: false,
      isPackaging: false,
    };
  }

  click() {
    alert("click");
  }

  package = () => {
    this.setState({ isLogDecrypting: false, isPackaging: true });
  };

  decrypt = () => {
    this.setState({ isPackaging: false, isLogDecrypting: true });
  };

  onClick = () => {
    async function clear() {
      const res = await axios.get(
        /*"https://10.38.166.102:3000/clear"*/ `${API}/clear`
      );
      console.log(res.data);
    }
    clear();
  };

  render() {
    return (
      <AuthContext.Consumer>
        {(context) => {
          return context.token ? (
            <Layout>
              <Sider
                theme="dark"
                breakpoint="lg"
                collapsedWidth="0"
                onBreakpoint={(broken) => {
                  console.log(broken);
                }}
                onCollapse={(collapsed, type) => {
                  console.log(collapsed, type);
                }}
                style={{ backgroundColor: "rgb(38, 43, 56)" }} //"rgb(36, 45, 49)"
                width={"150"}
              >
                <div
                  className="logo"
                  style={{
                    backgroundColor: "rgb(238, 112, 46)",
                    display: "table-cell",
                    textAlign: "center",
                  }}
                >
                  <img
                    src="https://cdn.cnbj1.fds.api.mi-img.com/mih5/fusion/header/static/media/6d6ace2678452cc103690a05df4effd5.png"
                    height="64px"
                    style={{ margin: "0 auto" }}
                    alt="logo"
                  />
                </div>
                <Menu
                  theme="dark"
                  mode="inline"
                  defaultSelectedKeys={["4"]}
                  style={{ backgroundColor: "rgb(38, 43, 56)" }}
                  // backgroundColor: "rgb(36, 45, 49)",
                >
                  <Menu.Item
                    style={{
                      marginTop: "5em",
                      textAlign: "center",
                      fontSize: "18px",
                    }}
                    className="menuItem"
                    key="1"
                    icon={
                      <InboxOutlined
                        style={{
                          fontSize: "18px",
                          color: "rgb(238, 112, 46)", //"#08c",
                          verticalAlign: "middle",
                        }}
                      />
                    }
                    onClick={this.package}
                  >
                    资源打包
                  </Menu.Item>
                  <Menu.Item
                    style={{
                      marginTop: "2em",
                      textAlign: "center",
                      fontSize: "18px",
                    }}
                    className="menuItem"
                    key="2"
                    icon={
                      <FileZipOutlined
                        style={{
                          fontSize: "18px",
                          color: "rgb(238, 112, 46)", //"#08c",
                          verticalAlign: "middle",
                        }}
                      />
                    }
                    onClick={this.decrypt}
                  >
                    日志解密
                  </Menu.Item>
                </Menu>
              </Sider>
              <Layout>
                <Header
                  className="site-layout-sub-header-background"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    textAlign: "center",
                    fontSize: "1.5em",
                    fontWeight: 700,
                    backgroundColor: "rgb(37, 43, 57)", //"rgb(94, 94, 163)",
                    color: "#fff",
                  }}
                >
                  米家工具
                  <div
                    style={{
                      position: "absolute",
                      top: "0em",
                      right: "0.5em",
                      borderRadius: "12%",
                      color: "rgb(255, 255, 255)",
                    }}
                  >
                    <Button
                      id="logoutButton"
                      size="large"
                      shape="circle"
                      onClick={context.logout}
                      style={{ display: "inline-block", color: "#fff" }}
                    >
                      退出登录
                    </Button>
                  </div>
                </Header>
                <Content
                  style={{
                    margin: "24px 16px 0",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  {!this.state.isPackaging && !this.state.isLogDecrypting && (
                    <>
                      <div
                        style={{
                          fontSize: "1.5em",
                          color: "#778899",
                          textAlign: "center",
                        }}
                      >
                        打包服务试运行
                      </div>
                      <br />
                      <Button
                        id="clearButton"
                        type="default"
                        onClick={this.onClick}
                      >
                        清除历史记录
                      </Button>
                    </>
                  )}
                  {this.state.isPackaging && <Pack />}
                  {this.state.isLogDecrypting && <FileUpload />}
                </Content>
                <Footer
                  style={{
                    textAlign: "center",
                    fontSize: "1.5em",
                    color: "gray",
                  }}
                >
                  MIOT
                </Footer>
              </Layout>
            </Layout>
          ) : null;
        }}
      </AuthContext.Consumer>
    );
  }
}

export default Home;
