import React from 'react';

const ListItems = ({ items }) => {
    if (!items || items.length === 0) return null;
    return (
        items.map((item, index) => {
            // return <li key={index}>{item.split("下载地址:")[0] + item.split("下载地址:")[1]}</li>
            const info = item.split("下载地址:")[0];
            const url = item.split("下载地址:")[1];
            if (!info || !url) return null;
            return <li key = {index}>
                <label>{info}下载地址:</label>
                <a target="_blank" rel="noreferrer" href={url.trim()}>{url.trim()}</a>
            </li>
        })
    )
}

export default ListItems;