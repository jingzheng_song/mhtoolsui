import React, { useState } from "react";
import Message from "./Message";
import Progress from "./Progress";
import axios from "axios";
import { Button } from "antd";
import "antd/dist/antd.css";
import "../styles/fileUpload.css";

const API = "https://mhtoolsapi.iot.mi.com";

const FileUpload = () => {
  const [file, setFile] = useState("");
  const [fileName, setFileName] = useState("选择文件");
  const [uploadedFile, setUploadedFile] = useState({});
  const [message, setMessage] = useState("");
  const [ready, setReady] = useState(false);
  const [uploadPercentage, setUploadPercentage] = useState(0);

  const onChange = (e) => {
    setFile(e.target.files[0]);
    setFileName(e.target.files[0].name);
  };

  const onSubmit = async (e) => {
    if (!file) {
      setMessage("没有选中文件！");
    } else {
      e.preventDefault();
      const formData = new FormData();
      formData.append("file", file);

      try {
        // 需要改地址
        const res = await axios.post(
          /*"https://10.38.166.102:3000/upload"*/ `${API}/upload`,
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
            onUploadProgress: (progressEvent) => {
              setUploadPercentage(
                parseInt(
                  Math.round((progressEvent.loaded * 100) / progressEvent.total)
                )
              );
              setTimeout(() => setUploadPercentage(0), 15000);
            },
          }
        );
        if (res.status === 400) {
          return;
        } else {
          const { fileName, filePath, ready } = res.data;
          console.log(fileName, filePath);

          setUploadedFile({ fileName, filePath });
          setReady(ready);
          setMessage("文件上传成功");
        }
      } catch (err) {
        if (err) {
          console.log(err);
          setMessage("There was a problem with the server");
        } else {
          setMessage(err.message);
        }
      }
    }
  };

  const onClick = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    try {
      // 需要改地址
      const res = await axios.get(
        /*"https://10.38.166.102:3000/parse"*/ `${API}/parse`,
        {
          params: {
            fileName: uploadedFile.fileName,
            filePath: uploadedFile.filePath,
          },
        }
      );
      let blob = res.data;
      let downloadElement = document.createElement("a");
      let href = window.URL.createObjectURL(
        new Blob([blob], { type: "application/pdf" })
      ); // 创建下载的链接
      downloadElement.href = href;
      downloadElement.download = `decrypt_${uploadedFile.fileName}.txt`;
      // downloadElement.download = time.getTime() + '.txt'; // 下载后文件名
      document.body.appendChild(downloadElement);
      downloadElement.click(); // 点击下载
      document.body.removeChild(downloadElement); // 下载完成移除元素
      window.URL.revokeObjectURL(href); // 释放掉blob对象
      setReady(false);
      setUploadPercentage(0);
    } catch (err) {
      /*if (err.response.status && err.response.status === 500) {
                console.log('There was a problem with the server');
            } else {
                console.log(err);
            }*/
      console.log(err);
    }
  };

  const onDismiss = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    try {
      // 需要改地址
      const res = await axios.get(
        /*"https://10.38.166.102:3000/parse"*/ `${API}/parse`,
        {
          params: {
            fileName: uploadedFile.fileName,
            filePath: uploadedFile.filePath,
          },
        }
      );
      console.log(res);
      setReady(false);
      setUploadPercentage(0);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="container">
      {message ? <Message msg={message} /> : null}
      {ready ? null : (
        <form onSubmit={onSubmit}>
          <div className="custom-file">
            <input
              type="file"
              className="custom-file-input"
              id="customFile"
              onChange={onChange}
            />
            <label className="custom-file-label" htmlFor="customFile">
              {fileName}
            </label>
          </div>

          <div style={{ marginTop: "1rem" }}>
            <Progress percentage={uploadPercentage} />
          </div>

          <input
            type="submit"
            value="上传"
            className="btn btn-warning btn-block mt-4"
            style={{
              backgroundColor: "rgb(38, 43, 56)",
              border: "none",
              color: "#fff",
            }}
          />
        </form>
      )}
      {uploadedFile ? (
        <div className="row mt-5">
          <div className="col-md-6 m-auto">
            <h4 style={{ color: "gray" }} className="text-center">
              {ready ? uploadedFile.fileName + " 已完成 请下载附件" : ""}
            </h4>
          </div>
        </div>
      ) : null}

      <div style={{ height: "2em" }}></div>

      {ready ? (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Button
            style={{ borderRadius: "1rem" }}
            type="primary"
            onClick={onClick}
          >
            下载
          </Button>
          <div style={{ width: "2em" }}></div>
          <Button
            style={{ borderRadius: "1rem" }}
            type="secondary"
            onClick={onDismiss}
          >
            放弃
          </Button>
        </div>
      ) : null}
    </div>
  );
};

export default FileUpload;
