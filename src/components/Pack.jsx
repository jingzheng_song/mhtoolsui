import React, { useState, useEffect } from "react";
import Message from "./Message";
import ListItems from './ListItems';
import axios from "axios";
import { Button } from "antd";
import "antd/dist/antd.css";
import "../styles/pack.css";

// 接口地址
const API = "https://mhtoolsapi.iot.mi.com";

const jenkins1 = "https://api.bq04.com/apps/latest/5c64c7e1ca87a82dc88f257c?api_token=ce8c5bc4174a562917c538fc704d90c1";
const jenkins2 = "https://api.bq04.com/apps/5c64c7e1ca87a82dc88f257c?api_token=ce8c5bc4174a562917c538fc704d90c1";

const Pack = () => {
  const [message, setMessage] = useState("");
  const [packReady, setPackReady] = useState(false);
  const [device, setDevice] = useState("");
  const [base, setBase] = useState("");
  const [tag, setTag] = useState("");
  const [firChecks, setFirChecks] = useState([]);
  const [downloadUrls, setDownLoadUrls] = useState([]);

  useEffect(() => {
    const packReady = (localStorage.getItem("packReady") === 'true' ? true : false) || false;
    const device = localStorage.getItem("device") || "";
    const base = localStorage.getItem("base") || "";
    const tag = localStorage.getItem("tag") || "";
    const firChecks = JSON.parse(localStorage.getItem("firChecks")) || [];
    const downloadUrls = JSON.parse(localStorage.getItem("downloadUrls")) || [];
    setPackReady(packReady);
    setDevice(device);
    setBase(base);
    setTag(tag);
    setFirChecks(firChecks);
    setDownLoadUrls(downloadUrls);
    const interval = setInterval(getFir, 30000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    localStorage.setItem("firChecks", JSON.stringify(firChecks));
  }, [firChecks]);

  useEffect(() => {
    localStorage.setItem("downloadUrls", JSON.stringify(downloadUrls));
  }, [downloadUrls]);

  useEffect(() => {
    localStorage.setItem("packReady", packReady);
  }, [packReady])

  const onSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("device", device);
    formData.append("base", base);
    formData.append("tag", tag);

    const firCheck = `company:jenkins_${device}_${tag}`;

    if (firChecks.indexOf(firCheck) !== -1) {
      setMessage("已创建该打包任务！ 请耐心等待！");
    } else {
      try {
        // 需要改地址
        const res = await axios.post(
          /*"http://localhost:3000/pack"*/ `${API}/pack`,
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        );
        if (res.status === 200) {
          const firCheck = `company:jenkins_${device}_${tag}`;
  
          setFirChecks([...firChecks, firCheck]);
  
          console.log(res);
  
          const { ready } = res.data;
  
          setPackReady(ready);
  
          setMessage("已发送");
        } else {
          console.log(res.status);
        }
      } catch (err) {
        if (err.code === 500) {
          setMessage("There was a problem with the server");
        } else {
          setMessage(err.message);
        }
      }
    }
  };

  const onCheck = (e) => {
      setPackReady(true);
      getFir();
  };

  const onClear = (e) => {
    setPackReady(false);
    setFirChecks([]);
    setDownLoadUrls([]);
  }

  const onBack = async (e) => {
    setPackReady(false);
  };

  const onDeviceChange = (e) => {
    localStorage.setItem("device", e.target.value);
    setDevice(e.target.value);
  };

  const onBaseChange = (e) => {
    localStorage.setItem("base", e.target.value);
    setBase(e.target.value);
  };

  const onTagChange = (e) => {
    localStorage.setItem("tag", e.target.value);
    setTag(e.target.value);
  };

  const getFir = async (e) => {
    try {
      const res = await axios.get(jenkins1);
      console.log(res);
      if (res.status === 200) {
        const data = res.data;
        let checklog = "";
        const changelog = data.changelog;
        console.log("changelog:", changelog);
        // const firCheck = "company:bugfix/data_dump";
        // const firCheck = `company:jenkins_${device}_${tag}`;
        for (let firCheck of firChecks) {
          console.log(firCheck);
          if (changelog && changelog.startsWith(firCheck)) {
            checklog = firCheck;
          }
        }
        console.log(checklog);
        if (checklog) {
          const res2 = await axios.get(jenkins2);
          console.log("res2", res2);
          if (res2.status === 200) {
            let masterReleaseId = res2.data.master_release_id;
            let url = `${changelog}下载地址: http://d.7short.com/2838?release_id=${masterReleaseId}`;
            if (downloadUrls === [] || downloadUrls.length === 0) {
              setDownLoadUrls([url]);
            } else if (downloadUrls.indexOf(url) === -1) {
              setDownLoadUrls([...downloadUrls, url]);
            }
          }
        } else {
          console.log("没有打包历史");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="container">
      {message ? <Message msg={message} /> : null}
      {packReady ? null : (
        <form onSubmit={onSubmit} style={{ width: "100%", height: "100%" }}>
          <div className="form-group">
            <label style={{color: 'rgb(39, 43, 55)'}}>Device</label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder={"ChuangMi"}
              checked
              required
              value={device}
              onChange={onDeviceChange}
            />
            <br />
            <label style={{color: 'rgb(39, 43, 55)'}}>Tag</label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder={"0.0.1"}
              required
              value={tag}
              onChange={onTagChange}
            />
            <br />
            <label style={{color: 'rgb(39, 43, 55)'}}>Base</label>
            <input
              type="text"
              className="form-control"
              id="formGroupExampleInput"
              placeholder={"master"}
              required
              value={base}
              onChange={onBaseChange}
            />
          </div>

          {firChecks.length > 0 ? <Button
          style={{ height: "2.5em", fontSize: "1.2em", backgroundColor: 'rgb(83, 185, 157)' }}
          onClick={onCheck}
          block
        >
          查看进度
        </Button> : <input
            type="submit"
            value="打包"
            className="btn btn-warning btn-block mt-4"
            style={{backgroundColor: 'rgb(38, 43, 56)', border: 'none', color: '#fff', height: '2.5em'}}
          />}
        </form>
      )}

      {packReady ? 
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <h3 style={{color: 'rgb(110, 183, 158)', fontStyle: 'italic'}}>当前任务数: {firChecks.length}</h3>
          <ListItems items={[...downloadUrls]}/>
        </div> : null}

      <div style={{ height: "2em" }}></div>

      {packReady ? 
      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
        <Button
          id="backButton"
          style={{ borderRadius: '1rem'}}
          onClick={onBack}
        >
          返回上级
        </Button>
        <div style={{ width: "20px" }}></div>
        <Button style={{ borderRadius: '1rem'}} type="secondary" onClick={onClear} >
          清空任务
        </Button>
      </div> : null}
    </div>
  );
};

export default Pack;
