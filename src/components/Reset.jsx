import React from "react";

import { Form, Button } from "antd";
import "antd/dist/antd.css";
import "../styles/fileForm.css";

class Reset extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      // filePath: ""
    };
  }

  render() {
    return (
      <Form className="form" name="nest-messages">
        <Form.Item className="button">
          <Button type="primary">清空记录</Button>
        </Form.Item>
      </Form>
    );
  }
}
export default Reset;
