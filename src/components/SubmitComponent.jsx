import React, { Component } from "react";
// import ReactDOM from 'react-dom';
import { post } from "axios";

class SubmitComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "",
    };
  }

  onChange(e) {
    let files = e.target.files;
    // console.warn('data file', files);
    let reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = (e) => {
      console.warn("file data", e.target.result);
      const url = "http://localhost:3000/upload";
      const formData = { file: e.target.result };
      return post(url, formData).then((response) =>
        console.warn("result", response)
      );
    };
  }

  render() {
    return (
      /*<div onSubmit={this.onFormSubmit}>
                <input type="file" name="file" onChange={(e) => this.onChange(e)}/>
                <input type="submit" value="upload"/>
            </div>*/
      <div>
        <form
          action="http://127.0.0.1:3000/upload"
          method="post"
          encType="multipart/form-data"
        >
          <input type="file" name="file" />
          <input type="submit" value="上传" />
        </form>
      </div>
    );
  }
}

export default SubmitComponent;
