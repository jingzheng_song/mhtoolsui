import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Home from "./Home";
import FileUpload from "./components/FileUpload";
import Pack from "./components/Pack";
import Login from "./Login";

import AuthContext from "./context/auth-context";

import Cookies from "js-cookie";

import "./App.css";

const App = () => {
  const [token, setToken] = useState(null);

  const login = (token, tokenExpiration) => {
    setToken(token);
  };

  const logout = () => {
    Cookies.remove("token");
    Cookies.remove("tokenExpiration");
    setToken(null);
  };

  useEffect(() => {
    const token = Cookies.get("token");
    const tokenExpiration = Cookies.get("tokenExpiration");
    if (token) {
      login(token, tokenExpiration);
    }
  }, []);

  return (
    <AuthContext.Provider
      value={{ token: token, login: login, logout: logout }}
    >
      <Router>
        <Switch>
          {token && <Route path="/home" exact component={Home} />}
          {token && <Route path="/upload" exact component={FileUpload} />}
          {token && <Route path="/pack" exact component={Pack} />}
          {token && <Route path="/" exact component={Home} />}
          {!token && <Route path="/login" component={Login} />}
          {!token && <Redirect to="/login" exact />}
          <Redirect to="/home" />
        </Switch>
      </Router>
    </AuthContext.Provider>
  );
}; /*<div className="container mt-4">
    <h4 className="display-4 text-center mb-4">
      <i className="fab fa-react"></i> File Upload
    </h4>

    <FileUpload />
  </div>*/

export default App;
