touch ~/.bashrc
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
source ~/.bashrc
nvm --version
nvm install v14.15.0
npm -v
node -v
npm install
npm run build
npm install -g serve
serve -l 80 -s build